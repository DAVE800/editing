import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AladinmanagerComponent } from './aladinmanager/aladinmanager.component';

const routes: Routes = [
  {path:'manager',component:AladinmanagerComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AladinclothesRoutingModule { }
