import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AladingadgetsRoutingModule } from './aladingadgets-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AladingadgetsRoutingModule
  ]
})
export class AladingadgetsModule { }
