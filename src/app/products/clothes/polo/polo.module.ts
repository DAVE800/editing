import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnfantsComponent } from './enfants/enfants.component';
import { FemmesComponent } from './femmes/femmes.component';
import { HommesComponent } from './hommes/hommes.component';



@NgModule({
  declarations: [
    EnfantsComponent,
    FemmesComponent,
    HommesComponent
  ],
  imports: [
    CommonModule
  ]
})
export class PoloModule { }
