import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClothesRoutingModule } from './clothes-routing.module';
import { ClothesmanagerComponent } from './clothesmanager/clothesmanager.component';
import { ChasublesComponent } from './chasubles/chasubles.component';
import { ImportcreaComponent } from './importcrea/importcrea.component';
import { DescriptionComponent } from './description/description.component';



@NgModule({
  declarations: [
    ClothesmanagerComponent,
    
    ChasublesComponent,
    ImportcreaComponent,
    DescriptionComponent
  ],
  imports: [
    CommonModule,
    ClothesRoutingModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  
})
export class ClothesModule { }
