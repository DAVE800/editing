import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportcreaComponent } from './importcrea.component';

describe('ImportcreaComponent', () => {
  let component: ImportcreaComponent;
  let fixture: ComponentFixture<ImportcreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImportcreaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportcreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
