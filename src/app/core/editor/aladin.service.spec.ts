import { TestBed } from '@angular/core/testing';

import { AladinService } from './aladin.service';

describe('AladinService', () => {
  let service: AladinService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AladinService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
