import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AladinProductsClothesComponent } from './aladin-products-clothes.component';

describe('AladinProductsClothesComponent', () => {
  let component: AladinProductsClothesComponent;
  let fixture: ComponentFixture<AladinProductsClothesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AladinProductsClothesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AladinProductsClothesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
