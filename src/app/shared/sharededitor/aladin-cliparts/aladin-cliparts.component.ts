import { Component, OnInit ,Output, EventEmitter } from '@angular/core';
import { ListService } from 'src/app/core';

@Component({
  selector: 'app-aladin-cliparts',
  templateUrl: './aladin-cliparts.component.html',
  styleUrls: ['./aladin-cliparts.component.scss']
})
export class AladinClipartsComponent implements OnInit {
  clipart:any
  current_page:any=1
  show=false;
  pages=[1,2,3];
  @Output () newItemEvent= new EventEmitter<string>();
  constructor(private http:ListService) { }

  ngOnInit(): void {

    this.http.getclipart(this.current_page).subscribe(res=>{
      this.clipart=res;

      console.log(this.clipart.cliparts)
    this.clipart=this.clipart.cliparts;
  },
  er=>{console.log(er)})

}


addNewItem(event:any){
  this.newItemEvent.emit(event)
}

toggle(){
  this.show=!this.show
}

currentPage(event:any){
  var page=event.target.id
    if(+page){
      this.current_page=page
      this.toggle()
      this.http.getclipart(page).subscribe(res=>{
        let data:any=res;
        if(data.status==200){
          if(data.cliparts.length>0){
            this.toggle()
            this.clipart=data.cliparts
          }else{
            this.toggle()
          }
        
        }else{
          this.toggle()
        }
      }, er=>{
        this.toggle()
        console.log(er)})
    }

}

nextPage(){
  this.current_page = this.current_page + 1
  this.toggle()
  this.http.getclipart(this.current_page).subscribe(res=>{
    let data:any=res;
    if(data.status==200){
      if(data.cliparts.length>0){
        this.toggle()
        this.clipart=data.cliparts
      }else{
        this.toggle()
      }
    
    }else{
      this.toggle()
    }

}, er=>{
  this.toggle()
  console.log(er)})
}

previousPage(){
  if(+this.current_page==1){

  }else{
    this.current_page = (+this.current_page) - 1
    this.toggle()
    this.http.getclipart(this.current_page).subscribe(res=>{
      this.clipart=res;
      if(this.clipart.status==200){
        this.toggle()
        this.clipart=this.clipart.cliparts;

      }
    }, er=>{
      this.toggle()
      console.log(er)})
  }

}


}
