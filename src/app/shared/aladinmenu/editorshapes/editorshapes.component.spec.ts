import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorshapesComponent } from './editorshapes.component';

describe('EditorshapesComponent', () => {
  let component: EditorshapesComponent;
  let fixture: ComponentFixture<EditorshapesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditorshapesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorshapesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
