import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditortextComponent } from './editortext.component';

describe('EditortextComponent', () => {
  let component: EditortextComponent;
  let fixture: ComponentFixture<EditortextComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditortextComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditortextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
